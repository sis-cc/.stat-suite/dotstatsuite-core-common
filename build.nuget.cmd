@echo OFF

if "%NUGET_ROOT%"=="" goto WARN

rmdir /S/Q "%userprofile%\.nuget\packages\dotstat.common"
rmdir /S/Q "%TEMP%\DotStat.Common"
dotnet pack %~dp0\DotStat.Common -c Debug -o "%TEMP%\DotStat.Common"
move "%TEMP%\DotStat.Common\*.nupkg" "%NUGET_ROOT%"

@echo Userprofile = %userprofile%
@echo NUGET_ROOT = %NUGET_ROOT%

dir %NUGET_ROOT%

goto :eof

:WARN
echo -----------------------------------------
echo Please set NUGET_ROOT environment variable with the path to your local nuget packages
echo -----------------------------------------