﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace DotStat.Common.Util
{
    public static class EncodingHelper
    {
        // ReSharper disable once StringLiteralTypo
        private static readonly string UnreservedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.~";

        public static string UrlEncode(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            var sb = new StringBuilder();

            foreach (char c in value)
            {
                if (UnreservedCharacters.IndexOf(c) != -1)
                {
                    sb.Append(c);
                }
                else
                {
                    sb.AppendFormat("%{0:x2}", (int)c);
                }
            }
            return sb.ToString();
        }

        public static bool GetIsBase64Encoded(string s)
        {
            s = s.Trim();
            return s.Length % 4 == 0 && Regex.IsMatch(s, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);
        }

        public static string Base64Encode(string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                var bytes = Encoding.UTF8.GetBytes(s);
                return Convert.ToBase64String(bytes);
            }

            return s;
        }

        public static string Base64Decode(string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                var bytes = Convert.FromBase64String(s);
                return Encoding.UTF8.GetString(bytes);
            }

            return s;
        }
    }
}