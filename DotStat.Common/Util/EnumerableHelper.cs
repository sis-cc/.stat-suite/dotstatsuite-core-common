﻿using System.Collections.Generic;

namespace DotStat.Common.Util
{
    public static class EnumerableHelper
    {
        public static IEnumerable<T> AsEnumerable<T>(this T value)
        {
            yield return value;
        }
    }
}
