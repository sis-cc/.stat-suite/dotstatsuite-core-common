﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using log4net;
using log4net.Core;
using Microsoft.AspNetCore.Http;

namespace DotStat.Common.Logger
{
    public static class Log
    {
        private static IHttpContextAccessor _httpContextAccessor;

        public static void Configure(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        private static HttpContext Current => _httpContextAccessor.HttpContext;

        /// <summary>
        /// Logs an info message 
        /// </summary>
        /// <param name="message">Message to be logged</param>
        /// <param name="memberName"></param>
        /// <param name="filePath"></param>
        /// <param name="lineNumber"></param>
        public static void Info(string message,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string filePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int lineNumber = -1)
        {
            var loggerName = GetLoggerName();
            var log = LogManager.GetLogger(Assembly.GetExecutingAssembly(), loggerName);
            if (!log.IsInfoEnabled)
            {
                return;
            }

            SetCustomProperties(memberName, filePath, lineNumber);
            log.Info(message);
        }

        /// <summary>
        /// Logs a notice message 
        /// </summary>
        /// <param name="message">Message to be logged</param>
        /// <param name="memberName"></param>
        /// <param name="filePath"></param>
        /// <param name="lineNumber"></param>
        public static void Notice(string message,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string filePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int lineNumber = -1)
        {
            var loggerName = GetLoggerName();
            var log = LogManager.GetLogger(Assembly.GetExecutingAssembly(), loggerName);
            if (!log.Logger.IsEnabledFor(Level.Notice))
            {
                return;
            }

            SetCustomProperties(memberName, filePath, lineNumber);
            log.Logger.Log(typeof(LogImpl), Level.Notice, message, null);
        }

        /// <summary>
        /// Logs an warning message
        /// </summary>
        /// <param name="message">Message to be logged</param>
        /// <param name="memberName"></param>
        /// <param name="filePath"></param>
        /// <param name="lineNumber"></param>
        public static void Warn(string message,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string filePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int lineNumber = -1)
        {
            var loggerName = GetLoggerName();
            var log = LogManager.GetLogger(Assembly.GetExecutingAssembly(), loggerName);
            if (!log.IsWarnEnabled)
            {
                return;
            }

            SetCustomProperties(memberName, filePath, lineNumber);
            log.Warn(message);
        }

        /// <summary>
        /// Logs an debug message 
        /// </summary>
        /// <param name="message">Message to be logged</param>
        /// <param name="memberName"></param>
        /// <param name="filePath"></param>
        /// <param name="lineNumber"></param>
        public static void Debug(string message,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string filePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int lineNumber = -1)
        {
            var loggerName = GetLoggerName();
            var log = LogManager.GetLogger(Assembly.GetExecutingAssembly(), loggerName);
            if (!log.IsDebugEnabled)
            {
                return;
            }

            SetCustomProperties(memberName, filePath, lineNumber);
            log.Debug(message);
        }

        /// <summary>
        /// Logs an exception message 
        /// </summary>
        /// <param name="exception">Exception to be logged</param>
        /// <param name="memberName"></param>
        /// <param name="filePath"></param>
        /// <param name="lineNumber"></param>
        public static void Error(Exception exception,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string filePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int lineNumber = -1)
        {
            var loggerName = GetLoggerName();
            var log = LogManager.GetLogger(Assembly.GetExecutingAssembly(), loggerName);
            if (!log.IsErrorEnabled)
            {
                return;
            }
            
            SetCustomProperties(memberName, filePath, lineNumber);
            log.Error(exception.Message, exception);
        }

        /// <summary>
        /// Logs an error message
        /// </summary>
        /// <param name="message">Message to be logged</param>
        /// <param name="memberName"></param>
        /// <param name="filePath"></param>
        /// <param name="lineNumber"></param>
        public static void Error(string message,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string filePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int lineNumber = -1)
        {
            var loggerName = GetLoggerName();
            var log = LogManager.GetLogger(Assembly.GetExecutingAssembly(), loggerName);
            if (!log.IsErrorEnabled)
            {
                return;
            }

            SetCustomProperties(memberName, filePath, lineNumber);
            log.Error(message);
        }


        /// <summary>
        /// Logs an fatal message 
        /// </summary>
        /// <param name="exception">Exception to be logged</param>
        /// <param name="memberName"></param>
        /// <param name="filePath"></param>
        /// <param name="lineNumber"></param>
        public static void Fatal(Exception exception,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string filePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int lineNumber = -1)
        {
            var loggerName = GetLoggerName();
            var log = LogManager.GetLogger(Assembly.GetExecutingAssembly(), loggerName);
            if (!log.IsFatalEnabled)
            {
                return;
            }

            SetCustomProperties(memberName, filePath, lineNumber);
            log.Fatal(exception.Message, exception);
        }

        public static void SetDataspaceId(string dataspaceId)
        {
            LogicalThreadContext.Properties[CustomParameter.DataSpaceId.ToString()] = dataspaceId;
        }

        public static void SetTransactionId(int transactionId)
        {
            LogicalThreadContext.Properties[CustomParameter.TransactionId.ToString()] = transactionId;
        }

        public static void SetUserEmail(string email)
        {
            LogicalThreadContext.Properties[CustomParameter.UserEmail.ToString()] = string.IsNullOrEmpty(email)? null : email;
        }

        private static void SetCustomProperties(string memberName, string filePath, int lineNumber)
        {
            LogicalThreadContext.Properties[CustomParameter.MemberName.ToString()] = memberName;
            LogicalThreadContext.Properties[CustomParameter.FilePath.ToString()] = filePath;
            LogicalThreadContext.Properties[CustomParameter.LineNumber.ToString()] = lineNumber;

            var parameters = new CustomParameters(Current);

            foreach (var customParameter in parameters.Parameters)
            {
                LogicalThreadContext.Properties[customParameter.Key.ToString()] = customParameter.Value;
            }
        }
        
        private static string GetLoggerName()
        {
            string loggerName = "unknown";

            StackTrace stackTrace = new StackTrace();

            if (stackTrace.FrameCount < 3)
            {
                return loggerName;
            }

            StackFrame frame = stackTrace.GetFrame(2);
            MethodBase method = frame.GetMethod();

            if (method != null)
            {
                loggerName =
                    $"{(method.DeclaringType != null ? method.DeclaringType.FullName : "unknown")}.{method.Name}";
            }

            return loggerName;
        }
        
    }
}
