﻿using DotStat.Common.Configuration;
using log4net;
using log4net.Appender;
using log4net.Repository.Hierarchy;
using System;
using System.Linq;
using System.Reflection;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Configuration.Interfaces;
using Google.Cloud.Logging.Log4Net;
using log4net.Core;
using log4net.Filter;
using log4net.Layout;

namespace DotStat.Common.Logger
{
    public static class LogHelper
    {
        private const string _dataspaceAppenderPlaceholder = "{0}DataspaceAppender";
        private const string _systemActivityAppenderName = "SystemActivityAppender";
        private const string _allActivityAppenderName = "AllActivityAppender";
        private const string _userActivityAppenderName = "UserActivityAppender";
        private const string _standardOutputAppenderName = "StandardOutputAppender";
        private const string _googleOutputAppenderName = "GoogleAppender";

        public static void ConfigureAppenders(BaseConfiguration configuration)
        {
            var systemActivityAppender = FindAppender(_systemActivityAppenderName);
            var allActivityAppender = FindAppender(_allActivityAppenderName);
            var userActivityAppender = FindAppender(_userActivityAppenderName);

            if (configuration.AutoLog2Stdout)
            {
                AddAppender(CreateConsoleAppender(configuration.AutoLog2StdoutLogLevel));
            }

            if (configuration.AutoLog2Google && !string.IsNullOrEmpty(configuration.GoogleProjectId))
            {
                AddAppender(CreateGoogleAppender(configuration));
            }

            if (systemActivityAppender != null)
            {
                AddSystemActivityFilters(systemActivityAppender as AppenderSkeleton);
            }

            if (configuration.SpacesInternal != null)
            {
                foreach (var dataspace in configuration.SpacesInternal)
                {
                    if (systemActivityAppender != null && dataspace.AutoLog2DB)
                    {
                        AddDataspaceFilter(dataspace.Id, systemActivityAppender as AppenderSkeleton);
                    }

                    if (allActivityAppender != null && dataspace.AutoLog2DB)
                    {
                        AddDataspaceFilter(dataspace.Id, allActivityAppender as AppenderSkeleton);
                    }

                    if (userActivityAppender != null && dataspace.AutoLog2DB)
                    {
                        AddDataspaceFilter(dataspace.Id, userActivityAppender as AppenderSkeleton);
                    }

                    var dataspaceAppender = FindAppender(string.Format(_dataspaceAppenderPlaceholder, dataspace.Id));

                    //Create a new dataspace appender if it was not configured in the log4net.config and AutoLogging is enabled
                    if (dataspaceAppender == null && dataspace.AutoLog2DB)
                    {
                        AddAppender(CreateDefaultDataspaceAppender(dataspace));
                    }
                    //If a dataspace appender exists with the proper name structure ({spaceid}DataspaceAppender)
                    //And the connection string has not been defined
                    //inject the connection string from the BaseConfiguration dataspaces.private.json
                    else if (dataspaceAppender != null && dataspaceAppender.GetType() == typeof(AdoNetAppender) &&
                             (dataspaceAppender as AdoNetAppender)?.ConnectionString == null)
                    {
                        ((AdoNetAppender)dataspaceAppender).ConnectionString =
                            dataspace.DotStatSuiteCoreDataDbConnectionString;
                        ((AdoNetAppender)dataspaceAppender).ActivateOptions();
                    }
                }
            }
        }

        public static void RecordNewTransaction(int transactionId, DataspaceInternal dataspace)
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly());
            hierarchy.Root.AddAppender(CreateMemoryAppender(transactionId, dataspace));

            Log.SetTransactionId(transactionId);
            Log.SetDataspaceId(dataspace.Id);
        }

        public static LoggingEvent[] GetRecordedEvents(int transactionId, string dataspaceId)
        {
            var name = $"{transactionId}_{dataspaceId}";

            var appender = (MemoryAppender)FindAppender(name);
            var events = appender.GetEvents();

            RemoveAppender(name);
            appender.Close();

            return events;
        }

        private static Level StringToLogLevel(string logLevel, Level defaultLevel)
        {
            var level = defaultLevel;

            var logggerRepository = LoggerManager.GetAllRepositories().FirstOrDefault();

            if (logggerRepository != null)
            {
                var levelMatch = logggerRepository.LevelMap[logLevel];

                if (levelMatch != null)
                {
                    level = levelMatch;
                }
            }

            return level;
        }

        private static AdoNetAppender CreateDefaultDataspaceAppender(DataspaceInternal dataspace)
        {
            var dataspaceId = dataspace.Id;
            var connectionString = dataspace.DotStatSuiteCoreDataDbConnectionString;
            var threshold = StringToLogLevel(dataspace.AutoLog2DBLogLevel, Level.Notice);
            string commandText;
            string connectionType;
            if (dataspace.DbType.Equals("MariaDb", StringComparison.OrdinalIgnoreCase)) {
                commandText =
                    "INSERT INTO `management_LOGS` (`Date`,`Thread`,`Transaction_Id`,`UserEmail`,`Level`,`Server`,`Application`,`Logger`,`Message`,`Exception`,`UrlReferrer`,`HttpMethod`,`RequestPath`,`QueryString`) VALUES (@log_date, @thread, @transactionId, @UserEmail, @log_level, @server, @application, @Logger, @message, @exception,@urlReferrer,@httpMethod,@requestPath,@queryString)";
                connectionType = "MySqlConnector.MySqlConnection, MySqlConnector";
            }
            else {
                commandText =
                    "INSERT INTO [Management].Logs ([Date],[Thread],[Transaction_Id],[UserEmail],[Level],[Server],[Application],[Logger],[Message],[Exception],[UrlReferrer],[HttpMethod],[RequestPath],[QueryString]) VALUES (@log_date, @thread, @transactionId, @UserEmail, @log_level, @server, @application, @Logger, @message, @exception,@urlReferrer,@httpMethod,@requestPath,@queryString)";
                connectionType = "System.Data.SqlClient.SqlConnection, System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";
            }

            var appender = new AdoNetAppender
            {
                Name = dataspaceId,
                UseTransactions = false,
                BufferSize = 1,
                ConnectionType = connectionType,
                CommandText = commandText,
                ConnectionStringName = $"ConnectionString-log-{dataspaceId}",
                ConnectionString = connectionString,
                Threshold = threshold,
                ReconnectOnError = true
            };

            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@log_date",
                DbType = System.Data.DbType.DateTime,
                Layout = new RawUtcTimeStampLayout()
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@thread",
                DbType = System.Data.DbType.String,
                Size = 255,
                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%thread"))
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = $"@{CustomParameter.DataSpaceId}",
                DbType = System.Data.DbType.String,
                Size = 255,
                Layout = new RawPropertyLayout() { Key = CustomParameter.DataSpaceId.ToString() }
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = $"@{CustomParameter.TransactionId}",
                DbType = System.Data.DbType.Int32,
                Layout = new RawPropertyLayout() { Key = CustomParameter.TransactionId.ToString() }
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = $"@{CustomParameter.UserEmail}",
                DbType = System.Data.DbType.String,
                Size = 350,
                Layout = new RawPropertyLayout() { Key = CustomParameter.UserEmail.ToString() }
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@log_level",
                DbType = System.Data.DbType.String,
                Size = 50,
                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%level"))
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@server",
                DbType = System.Data.DbType.String,
                Size = 50,
                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%property{log4net:HostName}"))
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = $"@{CustomParameter.Application}",
                DbType = System.Data.DbType.String,
                Size = 255,
                Layout = new RawPropertyLayout() { Key = CustomParameter.Application.ToString() }
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@Logger",
                DbType = System.Data.DbType.String,
                Size = 255,
                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%logger"))
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@message",
                DbType = System.Data.DbType.String,
                Size = -1,
                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%message"))
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = "@exception",
                DbType = System.Data.DbType.String,
                Size = -1,
                Layout = new Layout2RawLayoutAdapter(new ExceptionLayout())
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = $"@{CustomParameter.UrlReferrer}",
                DbType = System.Data.DbType.String,
                Size = -1,
                Layout = new RawPropertyLayout() { Key = CustomParameter.UrlReferrer.ToString() }
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = $"@{CustomParameter.HttpMethod}",
                DbType = System.Data.DbType.String,
                Size = -1,
                Layout = new RawPropertyLayout() { Key = CustomParameter.HttpMethod.ToString() }
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = $"@{CustomParameter.RequestPath}",
                DbType = System.Data.DbType.String,
                Size = -1,
                Layout = new RawPropertyLayout() { Key = CustomParameter.RequestPath.ToString() }
            });
            appender.AddParameter(new AdoNetAppenderParameter()
            {
                ParameterName = $"@{CustomParameter.QueryString}",
                DbType = System.Data.DbType.String,
                Size = -1,
                Layout = new RawPropertyLayout() { Key = CustomParameter.QueryString.ToString() }
            });
            appender.AddFilter(new ExactMatchPropertyFilter() 
            { 
                Key = CustomParameter.DataSpaceId.ToString(),
                StringToMatch = dataspaceId
            });
            appender.AddFilter(new DenyAllFilter());
            appender.ActivateOptions();
            return appender;
        }

        private static IAppender CreateMemoryAppender(int transactionId, DataspaceInternal dataspace)
        {
            var dataspaceId = dataspace.Id;

            var appender = new MemoryAppender
            {
                Name = $"{transactionId}_{dataspaceId}"
            };

            appender.Threshold = StringToLogLevel(dataspace.EmailLogLevel, Level.Notice);

            var filter = GetTransactionFilter("DotStat", transactionId, dataspaceId);
            appender.AddFilter(filter);

            filter = GetTransactionFilter("System.Data.SqlClient.SqlBulkCopy.FireRowsCopiedEvent", transactionId, dataspaceId);
            appender.AddFilter(filter);

            appender.AddFilter(new DenyAllFilter());

            appender.ActivateOptions();
            return appender;
        }

        private static AndFilter GetTransactionFilter(string loggerToMatch, int transactionId, string dataspace)
        {
            var filter = new AndFilter { AcceptOnMatch = true };
            filter.AddFilter(new LoggerMatchFilter
            {
                LoggerToMatch = loggerToMatch

            });
            filter.AddFilter(new ExactMatchPropertyFilter
            {
                Key = CustomParameter.TransactionId.ToString(),
                StringToMatch = transactionId.ToString()
            });
            filter.AddFilter(new ExactMatchPropertyFilter()
            {
                Key = CustomParameter.DataSpaceId.ToString(),
                StringToMatch = dataspace
            });
            return filter;
        }

        static IAppender CreateGoogleAppender(IGeneralConfiguration configuration)
        {
            var appender = new GoogleStackdriverAppender()
            {
                Name = _googleOutputAppenderName,
                ProjectId = configuration.GoogleProjectId,
                LogId = configuration.GoogleLogId ?? "DOTSTAT_BACKEND",
                Threshold = StringToLogLevel(configuration.GoogleLogLevel, Level.Debug),
                UsePatternWithinCustomLabels = true,
                Layout = new PatternLayout("%logger - %message"),
                DisableResourceTypeDetection = true
            };

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "thread",
                Value = "%thread"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "logger",
                Value = "%logger"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "exception",
                Value = "%exception"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "transactionId",
                Value = "%property{TransactionId}"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "dataspaceId",
                Value = "%property{DataSpaceId}"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "userEmail",
                Value = "%property{UserEmail}"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "server",
                Value = "%property{log4net:HostName}"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "urlReferrer",
                Value = "%property{UrlReferrer}"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "httpMethod",
                Value = "%property{HttpMethod}"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "requestPath",
                Value = "%property{RequestPath}"
            });

            appender.AddCustomLabel(new GoogleStackdriverAppender.Label()
            {
                Key = "queryString",
                Value = "%property{QueryString}"
            });

            appender.ActivateOptions();

            return appender;
        }

        private static void AddAppender(IAppender appender)
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly());
            hierarchy.Root.AddAppender(appender);
        }

        private static IAppender FindAppender(string name)
        {
            var appenders = LogManager.GetRepository(Assembly.GetExecutingAssembly()).GetAppenders();
            return appenders.FirstOrDefault(a => a.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        private static void RemoveAppender(string name)
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly());
            hierarchy.Root.RemoveAppender(name);
        }

        private static void AddDataspaceFilter(string dataspaceId, AppenderSkeleton appender)
        {
            appender.AddFilter(new ExactMatchPropertyFilter
            {
                Key = CustomParameter.DataSpaceId.ToString(),
                StringToMatch = dataspaceId,
                AcceptOnMatch = false
            });
        }

        private static void AddSystemActivityFilters(AppenderSkeleton appender)
        {
            appender.AddFilter(new PropertyFilter
            {
                Key = CustomParameter.HttpMethod.ToString(),
                StringToMatch = "GET",
                AcceptOnMatch = false
            });

            appender.AddFilter(new PropertyFilter
            {
                Key = CustomParameter.HttpMethod.ToString(),
                StringToMatch = "POST",
                AcceptOnMatch = false
            });

            appender.AddFilter(new LoggerMatchFilter
            {
                LoggerToMatch = "Org.Sdmxsource",
                AcceptOnMatch = false
            });

            appender.AddFilter(new LoggerMatchFilter
            {
                LoggerToMatch = "Estat.Sdmxsource",
                AcceptOnMatch = false
            });

            appender.AddFilter(new LoggerMatchFilter
            {
                LoggerToMatch = "Estat.Sri.MappingStoreRetrieval",
                AcceptOnMatch = false
            });

            appender.AddFilter(new LoggerMatchFilter
            {
                LoggerToMatch = "org.estat.sri.sqlquerylogger",
                AcceptOnMatch = false
            });
        }

        private static IAppender CreateConsoleAppender(string level)
        {
            ConsoleAppender appender = new ConsoleAppender
            {
                Name = _standardOutputAppenderName,
                Layout = new PatternLayout("%date|[%thread]|%property{DataSpaceId}|%property{TransactionId}|%property{UserEmail}|%-5level|%property{log4net:HostName}|%property{Application}|%logger|%message|%exception|%property{UrlReferrer}|%property{HttpMethod}|%property{RequestPath}|%property{QueryString}%newline")
            };
            appender.Threshold = StringToLogLevel(level, Level.Notice);
            return appender;
        }
    }
}
