﻿using System.Collections.Generic;
using DotStat.Common.Model;

namespace DotStat.Common.Auth
{
    public interface IAuthorizationRepository
    {
        IEnumerable<UserAuthorization> GetAll();
        IEnumerable<UserAuthorization> GetByUser(DotStatPrincipal principal);
        UserAuthorization GetById(int id);
        int Insert(UserAuthorization ua);
        int Update(UserAuthorization ua);
        int Delete(int id);
    }
}