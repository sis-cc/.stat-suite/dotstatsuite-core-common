﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace DotStat.Common.Auth
{
    public class DotStatPrincipal : ClaimsPrincipal
    {
        public readonly bool IsAnonymous = true;
        public readonly string Email = "-NONE-";
        public readonly string UserId = "-NONE-";
        public readonly IList<string> Groups = new string[0];

        public DotStatPrincipal(ClaimsPrincipal principal, Dictionary<string, string> map) : base(principal ?? new ClaimsPrincipal())
        {
            if (principal == null)
                return;

            IsAnonymous = false;

            Email = principal.Claims
                .FirstOrDefault(x => x.Type == GetClaim(map, "email"))
                ?.Value;

            UserId = Email;

            Groups = principal.Claims
                .Where(x => x.Type == GetClaim(map, "groups"))
                .Select(x => x.Value)
                .ToArray();
        }

        private string GetClaim(Dictionary<string, string> claimsMapping, string key)
        {
            return claimsMapping!=null && claimsMapping.ContainsKey(key) ? claimsMapping[key] : key;
        }
    }
}