﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace DotStat.Common.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class DotStatException : Exception
    {
        public DotStatException()
        {
        }

        public DotStatException(string message) : base(message)
        {
        }

        public DotStatException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}