﻿using System;
using System.Runtime.Serialization;
using Dapper;
using DotStat.Common.Enums;
using Estat.Sdmxsource.Extension.Constant;

namespace DotStat.Common.Model
{
    [Table("AuthorizationRules")]
    public class UserAuthorization : IDotStatEntity<int>
    {
        public UserAuthorization()
        {}

        public UserAuthorization(int id, string dataSpace, string agencyId, string artefactId, string version, PermissionType permission)
        {
            Id = id;
            DataSpace = dataSpace;
            ArtefactAgencyId = agencyId;
            ArtefactId = artefactId;
            ArtefactVersion = version;
            Permission = permission;
        }

        public int Id { get; set; }
        public string UserMask { get; set; }
        public bool IsGroup { get; set; }
        public string DataSpace { get; set; }
        public SDMXArtefactType ArtefactType { get; set; }
        public string ArtefactAgencyId { get; set; }
        public string ArtefactId { get; set; }
        public string ArtefactVersion { get; set; }
        public PermissionType Permission { get; set; }

        [IgnoreDataMember]
        public string EditedBy { get; set; }

        [IgnoreDataMember]
        public DateTime EditDate { get; set; }

        [IgnoreDataMember]
        [Editable(false)]
        public bool IsNew => Id == 0;
    }
}
