﻿namespace DotStat.Common.Messaging
{
    public class QueueItem
    {
        public string Action { get; set; }
        public string Dataspace { get; set; }
        public string PrincipalName { get; set; }
        public string PrincipaEmail { get; set; }
        public string ArtefactId { get; set; }
        public string ArtefactAgency { get; set; }
        public string ArtefactVersion { get; set; }

        public QueueItem()
        {
        }

        public QueueItem(
            string action,
            string dataspace,
            string principalname,
            string principalEmail,
            string artefactId,
            string artefactAgency,
            string artefactVersion)
        {
            this.Action = action;
            this.Dataspace = dataspace;
            this.PrincipalName = principalname;
            this.PrincipaEmail = principalEmail;
            this.ArtefactId = artefactId;
            this.ArtefactAgency = artefactAgency;
            this.ArtefactVersion = artefactVersion;

        }
    }
}
