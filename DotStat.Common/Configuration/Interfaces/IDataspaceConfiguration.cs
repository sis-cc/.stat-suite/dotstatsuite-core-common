﻿using System.Collections.Generic;
using DotStat.Common.Configuration.Dto;

namespace DotStat.Common.Configuration.Interfaces
{
    public interface IDataspaceConfiguration
    {
        IList<DataspaceInternal> SpacesInternal { get; set; }
        bool ShowAdvanceDbHealthInfo { get; set; }
    }
}
