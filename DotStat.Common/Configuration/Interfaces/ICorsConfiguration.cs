namespace DotStat.Common.Configuration.Interfaces
{
    public interface ICorsConfiguration
    {
        /// <summary>
        /// An array of allowed origin urls or IPs
        /// </summary>
        string[] CorsAllowedOrigins { get; set; }
    }
}
