namespace DotStat.Common.Configuration.Interfaces
{
    public interface IMailConfiguration
    {
        /// <summary>
        /// The name or the IP address of the SMTP server
        /// </summary>
        string SmtpHost { get; set; }
        /// <summary>
        /// The port of the SMTP server
        /// </summary>
        int SmtpPort { get; set; }
        /// <summary>
        /// Specify whether the SmtpClient uses Secure Sockets Layer (SSL) to encrypt the connection
        /// </summary>
        bool SmtpEnableSsl { get; set; }
        /// <summary>
        /// The user name used to authenticate on the smtp server
        /// </summary>
        string SmtpUserName { get; set; }
        /// <summary>
        /// The user password used to authenticate on the smtp server
        /// </summary>
        string SmtpUserPassword { get; set; }
        /// <summary>
        /// The 'from' mail address
        /// </summary>
        string MailFrom { get; set; }
        /// <summary>
        /// The value for the optional 'HFrom' header
        /// </summary>
        string SmtpHFrom { get; set; }
    }
}
