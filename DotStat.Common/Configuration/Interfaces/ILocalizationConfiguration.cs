﻿using System.Collections.Generic;

namespace DotStat.Common.Configuration.Interfaces
{
    public interface ILocalizationConfiguration
    {
        Dictionary<string, Dictionary<Localization.Localization.ResourceId,string>> LocalizedResources { get; set; }
    }
}
