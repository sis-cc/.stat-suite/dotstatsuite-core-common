﻿namespace DotStat.Common.Configuration.Interfaces
{
    public interface IGeneralConfiguration
    {
        /// <summary>
        /// Max amount of processing errors returned back from Transfer service
        /// </summary>
        int MaxTransferErrorAmount { get; set; }

        /// <summary>
        /// Default 2 letter language code
        /// </summary>
        string DefaultLanguageCode { get; set; }

        /// <summary>
        /// Connection string to Common DB, storing logs & auth rules
        /// </summary>
        string DotStatSuiteCoreCommonDbConnectionString { get; set; }

        /// <summary>
        /// Indicates what type of database connection should be used.
        /// The possible values of "DbType": "SqlServer", "MariaDb"
        /// Default: SqlServer
        /// </summary>
        public string DbType { get; set; }

        /// <summary>
        /// Log to standard output
        /// </summary>
        bool AutoLog2Stdout { get; set; }

        /// <summary>
        /// Log level for standard output
        /// </summary>
        string AutoLog2StdoutLogLevel { get; set; }

        /// <summary>
        /// Log to Google.Cloud 
        /// </summary>
        bool AutoLog2Google { get; set; }

        /// <summary>
        /// Log level for Google.Cloud
        /// </summary>
        string GoogleLogLevel { get; set; }

        /// <summary>
        /// ProjectId for Google.Cloud
        /// </summary>
        string GoogleProjectId { get; set; }

        /// <summary>
        /// LodId for Google.Cloud
        /// </summary>
        string GoogleLogId { get; set; }

        /// <summary>
        /// Maximum length of dimension with TextFormat textType="String"
        /// </summary>
        int MaxTextDimensionLength { get; set; }

        /// <summary>
        /// Directory to store temporary files for data imports/validations
        /// </summary>
        string TempFileDirectory { get; set; }

        /// <summary>
        /// Number of hours before a temporary file is deleted
        /// </summary>
        int TempFileMaxAgeInHours { get; set; }

        /// <summary>
        /// Minimum percentage of free available disk space in the TempFileDirectory to accept new requests
        /// </summary>
        int MinPercentageDiskSpace { get; set; }

        /// <summary>
        /// The timeout in seconds to wait before the http request times out
        /// </summary>
        int HttpClientTimeOut { get; set; }

        /// <summary>
        /// Maximum number of requests to be processed at the same time.
        /// </summary>
        int MaxConcurrentTransactions { get; set; }

        /// <summary>
        /// A unique id to identify the instance of the transfer service (max 20 characters)
        /// </summary>
        string ServiceId { get; set; }

        /// <summary>
        /// Indicates if the transfer service should include the values found for non coded dimensions during the calculation of the actual content constraint.
        /// </summary>
        bool ExcludeNonCodedDimensionsDuringConstraintCalculation { get; set; }
    }
}
