﻿using System.Collections.Generic;

namespace DotStat.Common.Configuration.Interfaces
{
    public interface IAuthConfiguration
    {
        /// <summary>
        /// Is openid authentication enabled
        /// </summary>
        bool Enabled { get; set; }
        /// <summary>
        /// Authority url of token issuer
        /// </summary>
        string Authority { get; set; }
        /// <summary>
        /// Client/application Id
        /// </summary>
        string ClientId { get; set; }
        /// <summary>
        /// Authorization url (used in swagger UI interface)
        /// </summary>
        string AuthorizationUrl { get; set; }
        /// <summary>
        /// Token url (used in swagger UI interface)
        /// </summary>
        string TokenUrl { get; set; }
        /// <summary>
        /// Requested openId scopes (used as parameters for authorization url)
        /// </summary>
        string[] Scopes { get; set; }
        /// <summary>
        /// Claims mapping rules
        /// </summary>
        Dictionary<string, string> ClaimsMapping { get; set; }
        /// <summary>
        /// Is HTTPS connection to OpenId authority server required
        /// </summary>
        bool RequireHttps { get; set; }
        /// <summary>
        /// Is iss (issuer) claim in JTW token should match configured authority
        /// </summary>
        bool ValidateIssuer { get; set; }
        /// <summary>
        /// Expected aud (audience) claim  in JWT token, if not set equals to ClientId 
        /// </summary>
        string Audience { get; set; }
    }
}
