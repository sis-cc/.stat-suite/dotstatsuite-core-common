﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotStat.Common.Auth;
using DotStat.Common.Model;
using Estat.Sdmxsource.Extension.Constant;
using Moq;
using NUnit.Framework;

namespace DotStat.Common.Test
{
    [TestFixture]
    public class AuthorizationManagementTests
    {
        private string _email;
        private IEnumerable<string> _groups;
        private DotStatPrincipal _principal;
        private AuthorizationManagement _management;
        private IList<UserAuthorization> _rules;

        public AuthorizationManagementTests()
        {
            _email = "mail@host.com";
            _groups = new[] {"GROUP1", "GROUP2", "GROUP3" };

            var claims = new List<Claim>()
            {
                new Claim("claim/email", _email),
            };

            foreach(var g in _groups)
                claims.Add(new Claim("claim/groups", g));

            var claimsMap = new Dictionary<string, string>()
            {
                {"email","claim/email"},
                {"groups","claim/groups"}
            };

            _principal = new DotStatPrincipal(new ClaimsPrincipal(new ClaimsIdentity(claims)), claimsMap);

            // -------------------------------------

            _rules = new List<UserAuthorization>()
            {
                new UserAuthorization(1, "design", "OECD", "DF1", "2.0", PermissionType.DataImporterRole),
                new UserAuthorization(2, "process", "ISTAT", "*", "*", PermissionType.AdminRole),
                new UserAuthorization(3, "disseminate", "*", "*", "*", PermissionType.CanReadData),
                new UserAuthorization(4, "*", "*", "*", "*", PermissionType.CanImportData),
            };

            // --------------------------------------------------------

            var repoMock = new Mock<IAuthorizationRepository>();

            repoMock
                .Setup(x => x.GetByUser(It.IsAny<DotStatPrincipal>()))
                .Returns(_rules);
            
            // --------------------------------------------------------

            _management = new AuthorizationManagement(repoMock.Object);
        }

        [Test]
        public void ClaimsMapping()
        {
            Assert.AreEqual(_email, _principal.Email);
            Assert.AreEqual(_email, _principal.UserId);
            Assert.IsNotNull(_principal.Groups);
            Assert.AreEqual(_groups.Count(), _principal.Groups.Count);
            Assert.IsTrue(_groups.All(x=>_principal.Groups.Contains(x)));
            Assert.IsFalse(_principal.IsAnonymous);
        }

        [Test]
        public void Anonymous()
        {
            var principal = new DotStatPrincipal(null, null);

            Assert.IsNotNull(principal);
            Assert.IsTrue(principal.IsAnonymous);
        }

        [Test]
        public void PrincipalWithDefaultMapping()
        {
            var claims = new[]
            {
                new Claim("email", "some@email.com"),
                new Claim("groups", "GROUP1"),
                new Claim("groups", "GROUP2")
            };

            var principal = new DotStatPrincipal(new ClaimsPrincipal(new ClaimsIdentity(claims)), null);

            Assert.IsNotNull(principal);
            Assert.AreEqual(claims.Length, principal.Claims.Count());
            Assert.AreEqual(claims[0].Value, principal.Email);
            Assert.AreEqual(2, principal.Groups.Count);
            Assert.AreEqual(claims[1].Value, principal.Groups[0]);
            Assert.AreEqual(claims[2].Value, principal.Groups[1]);
        }

        [Test]
        public void RulesLoad()
        {
            Assert.AreEqual(_rules.Count, _management.UserRules(_principal).Count());
        }

        [Test]
        public void RulesInsert()
        {
            // todo
        }

        [Test]
        public void RulesUpdate()
        {
            // todo
        }

        [Test]
        public void RulesDelete()
        {
            // todo
        }

        [TestCase("design","OECD","DF0","1.0",PermissionType.CanReadData, null)]
        [TestCase("design", "OECD", "DF1", "2.0",PermissionType.CanReadData, 1)]
        [TestCase("process", "ISTAT","DF2","3.0",PermissionType.AdminRole, 2)]
        [TestCase("process", "ISTAT","DF3","4.0",PermissionType.CanReadData, 2)]
        [TestCase("process", "SOME","DF3","4.0",PermissionType.CanReadData, null)]
        [TestCase("none", "ISTAT", "DF2", "3.0", PermissionType.CanReadData, null)]
        [TestCase("disseminate", "ISTAT", "DF2", "3.0", PermissionType.CanReadData, 3)]
        [TestCase("disseminate", "ISTAT", "DF3", "4.0", PermissionType.CanReadData, 3)]
        [TestCase("disseminate", "XXX", "DF3", "4.0", PermissionType.CanReadData, 3)]
        [TestCase("disseminate", "ISTAT", "DF3", "4.0", PermissionType.CanImportData, 4)]
        [TestCase("new", "ZZZ", "DF5", "10.0", PermissionType.CanImportData, 4)]
        [TestCase("new", "ZZZ", "DF5", "10.0", PermissionType.CanReadData, null)]
        public void IsAuthorized(string dataSpace, string agencyId, string artefactId, string version, PermissionType permission, int? expectedRuleId)
        {
            var rule = _rules.FirstOrDefault(x => _management.IsAuthorized(x, dataSpace, agencyId, artefactId, version, permission));

            Assert.AreEqual(expectedRuleId, rule?.Id);
        }
    }
}
