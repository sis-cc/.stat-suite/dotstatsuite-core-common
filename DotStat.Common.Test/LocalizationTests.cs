﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using NUnit.Framework;

namespace DotStat.Common.Test
{
    [TestFixture]
    public class LocalizationTests
    {
        private ILocalizationConfiguration _config;

        public LocalizationTests()
        {
            _config = new TestLocalizationConfiguration();
        }

        [Test, Order(1)]
        public void ConfigureLocalization()
        {
            Assert.Throws<ArgumentNullException>(() => LocalizationRepository.Configure(null));
            Assert.DoesNotThrow(() => LocalizationRepository.Configure(_config));
        }

        [Test, Order(2)]
        public void RetrieveExistingResource()
        {
            var translation = _config.LocalizedResources.FirstOrDefault();

            Assert.IsNotNull(translation);
            
            var value = LocalizationRepository.GetLocalisedResource(
                Localization.Localization.ResourceId.ChangeInDsdAttributeAttachmentGroupChanged,
                translation.Key
            );

            Assert.AreEqual(translation.Value[Localization.Localization.ResourceId.ChangeInDsdAttributeAttachmentGroupChanged], value);

            value = LocalizationRepository.GetLocalisedResource(
                Localization.Localization.ResourceId.ChangeInDsdAttributeAttachmentGroupChanged);

            Assert.AreEqual(translation.Value[Localization.Localization.ResourceId.ChangeInDsdAttributeAttachmentGroupChanged], value);
        }

        [Test, Order(3)]
        public void RetrieveNotExistingResource()
        {
            Assert.Throws<InvalidOperationException>(() => LocalizationRepository.GetLocalisedResource(
                Localization.Localization.ResourceId.NoActiveTransactionWithId,
                "xx"
            ));
        }

        private class TestLocalizationConfiguration : ILocalizationConfiguration
        {
            public Dictionary<string, Dictionary<Localization.Localization.ResourceId, string>> LocalizedResources
            {

                get => new Dictionary<string, Dictionary<Localization.Localization.ResourceId, string>>
                {
                    {
                        "en",
                        new Dictionary<Localization.Localization.ResourceId, string>
                        {
                            { Localization.Localization.ResourceId.ChangeInDsdAttributeAttachmentGroupChanged, $"This is a test" }
                        }
                    }
                };

                set => throw new System.NotImplementedException();
            }
        }

    }
}
